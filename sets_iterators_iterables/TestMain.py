from python3Learning.src.oopBasics.MyFuncs import section, description

def updateSet(s1, s2):
    s1.update([7, 8, 9], s2)
    return print(f"\n3.)"
                 f"s1.update([7, 8, 9], s2):"
                 f" {s1 = }")

def main():
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Sets-Example")
    
    # set(): A set is like a list but
    # it removes any duplicate values.
    s1 = {1, 2, 3, 4, 5, 5, 4, 3}
    
    # the set will remove the duplicates of
    # {5, 4, 3}, since they repeat in the set.
    print(f"1.) {s1 = } ")
    
    print()
    # ----------------------------------------------
    description("Adding values to the set")
    
    # Adding a new value &
    # multiple values to our set
    s1.add(6)
    print(f"2.) s1.add(6): {s1 = } ")
    
    s2 = {8, 9, 10}
    updateSet(s1, s2)

    # ----------------------------------------------
    description("Removing values from the set")
    # ----------------------------------------------

    # remove(1): If [1] wasn't a part of a the
    # set, a 'key' error would've been thrown
    s1.remove(1)
    print(f"4.) s1.remove(1): {s1 = } ")

    # s1.discard(1): Does not throw an error
    # if the value is not present in the set
    s1.discard(1)
    # Will return nothing but now key ERROR
    print(f"5.) s1.discard(1): {s1 = } ")
    
    print()
    # ----------------------------------------------
    description("intersection(), set")
    # ----------------------------------------------
    s3 = {1, 2, 3}
    s4 = {2, 3, 4}
    s5 = {3, 4, 5}
    
    # it will return the intersection of
    # 2 & 3 & discard 1 & 4 since they
    # do not intersect or are not the same
    s6 = s3.intersection(s4)
    print(f"6.) intersection(s4): {s6 = }")
    
    # The only number that intersect
    # with all three sets is 3
    s7 = s3.intersection(s4, s5)
    print(f"7.) s3.intersection(s4, s5): {s7 = }")
    
    print()
    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # The difference() method does the opposite of the intersection()
    # method. It checks for values that do not match at all. Also We
    # are testing against one set to what ever set is the difference.
    # So the difference set might not have a value of the set trying
    # to distinguish a value from its own, but that does not matter,
    # since the original set is the one running the test against any
    # of the set or other sets being tested
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s6 = s3.difference(s4)
    print(f"6.) s3.difference(s4): {s6 = }")

    s7 = s3.difference(s4, s5)
    print(f"7.) s3.difference(s4, s5): {s7 = }")


if __name__ == '__main__':
    main()
