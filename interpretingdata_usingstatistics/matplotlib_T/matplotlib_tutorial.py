from typing import List

from matplotlib import pyplot as plt

def main():
    """code-here ----------------------------------"""
    """ -----------------------------
       How to check styles available:
       styles = plt.style.available
       print(styles):
       Below after choosing from
       available, implement the style
    ------------------------------"""
    plt.style.use("fivethirtyeight")
    
    # Cartoonish style to the Languagedata.csv chart
    # plt.xkcd()
    
    # Our Data: Median salary
    # for a Dev Based on there age
    ages_x = [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35]
    # ------------------------------------------------------------
    py_dev_y: List[int] = [
            45372, 48876, 53850, 57287,
            63016, 65998, 70003, 70000,
            71496, 75370, 83640
            ]
    # Plotting our Languagedata.csv
    plt.plot(
            ages_x, py_dev_y,
            # color = 'blue',
            # linewidth = 3,
            label = 'Python'
            )
    # ------------------------------------------------------------
    js_dev_y: List[int] = [
            37810, 43515, 46823, 49293,
            53437, 56373, 62375, 66674,
            68745, 68746, 74583
            ]

    # Labeling our Languagedata.csv
    plt.xlabel("Ages")
    plt.ylabel("Median Salary ($USD)")
    plt.title("Median Salary ($USD) by Age")
    # Plotting our Languagedata.csv
    plt.plot(
            ages_x, js_dev_y,
            # color = 'orange',
            # linewidth = 3,
            label = 'Javascript'
            )
    # ------------------------------------------------------------
    dev_y: List[int] = [
            38496, 42000, 46752, 49320,
            53200, 56000, 62316, 64928,
            67317, 68748, 73752
            ]
    # Plotting our Languagedata.csv
    plt.plot(
            ages_x, dev_y,
            # color = 'purple',
            # linewidth = 3,
            linestyle = '--',
            label = 'All Devs'
            )
    # ------------------------------------------------------------

    plt.legend()
    
    # Adding a grid to the Languagedata.csv chart
    # plt.grid()
    
    # Adds padding to the Languagedata.csv chart
    plt.tight_layout()
    
    # If you do not want to save the Languagedata.csv chart,
    # when pycharm outputs it. You can save it,
    # with this function call
    # plt.savefig("cartoonPlot.png")
    
    plt.show()
    
    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
