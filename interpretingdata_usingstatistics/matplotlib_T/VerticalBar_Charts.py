from typing import List
import numpy as np
from matplotlib import pyplot as plt

def main():
    """code-here ----------------------------------"""
    """ -----------------------------
       How to check styles available:
       styles = plt.style.available
       print(styles):
       Below after choosing from
       available, implement the style
    ------------------------------"""
    plt.style.use("fivethirtyeight")
   
    # Our Data: Median salary
    # for a Dev Based on there age
    ages_x = [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35]
    # Numpy Array
    x_indexes = np.arange(len(ages_x))
    # Setting our bar my_width
    my_width = 0.25
    # ------------------------------------------------------------
    py_dev_y: List[int] = [45372, 48876, 53850, 57287, 63016, 65998,
                           70003, 70000, 71496, 75370, 83640]
    # Vertical Bar-chart of our Languagedata.csv
    plt.bar(x_indexes - my_width, py_dev_y, width = my_width, label = 'Python')
    # ------------------------------------------------------------
    js_dev_y: List[int] = [37810, 43515, 46823, 49293, 53437, 56373,
                           62375, 66674, 68745, 68746, 74583]
    # Vertical Bar-chart of our Languagedata.csv
    plt.bar(x_indexes, js_dev_y, width = my_width, label = 'Javascript')
    # ------------------------------------------------------------
    dev_y: List[int] = [38496, 42000, 46752, 49320,53200, 56000,
                        62316, 64928, 67317, 68748, 73752]
    # Vertical Bar-chart of our Languagedata.csv
    plt.bar(x_indexes + my_width, dev_y, width = my_width, label = 'All Devs')
    # ------------------------------------------------------------
    
    plt.legend()
    # So our x-axis does not use the
    # indexes, and uses the actual age
    plt.xticks(ticks = x_indexes, labels = ages_x)
    plt.title("Median Salary ($USD) By Age")
    plt.xlabel("Ages")
    plt.ylabel("Median Salary ($USD)")
    
    plt.show()
    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
