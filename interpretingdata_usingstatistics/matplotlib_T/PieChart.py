from typing import List, Any, Union

from matplotlib import pyplot as plt

def main():
    plt.style.use("seaborn-notebook")
    
    """code-here ----------------------------------"""
    # Language Popularity
    my_slices: List[Union[int, Any]] = [59219, 55466, 47544, 36443, 35917]
    my_labels: List[Union[str, Any]] = ['JavaScript', 'HTML/CSS', 'SQL', 'Python', 'Java']
    # List to infasize each slice in the list
    lets_explode = [0, 0, 0, 0.15, 0]
    
    plt.pie(
            x = my_slices,
            labels = my_labels,
            explode = lets_explode,
            shadow = True,
            startangle = 90,
            autopct = '%1.1f%%',
            # rotatelabels = True,
            wedgeprops = {"edgecolor": "black"}
            )
    
    plt.title("My Pie Chart")
    plt.tight_layout()
    
    plt.show()
    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
