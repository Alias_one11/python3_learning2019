from typing import List
from matplotlib import pyplot as plt
from matplotlib import dates as mpl_dates
import pandas as pd
from datetime import datetime, timedelta


def main():
    """code-here ----------------------------------"""
    plt.style.use("fivethirtyeight")
    
    dates: List[datetime] = [
            datetime(2019, 5, 24),
            datetime(2019, 5, 25),
            datetime(2019, 5, 26),
            datetime(2019, 5, 27),
            datetime(2019, 5, 28),
            datetime(2019, 5, 29),
            datetime(2019, 5, 30)
            ]
    
    y: List[int] = [0, 1, 3, 4, 6, 5, 7]
    
    plt.plot_date(dates, y)
    
    # data = pd.read_csv("Stock_data.csv")
    # price_date = data["Date"]
    # price_close = data["Close"]
    #
    # plt.title("Bitcoin Prices")
    # plt.xlabel("Date")
    # plt.ylabel("Closing Price")
    
    plt.tight_layout()
    plt.show()
    
    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
