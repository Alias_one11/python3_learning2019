import pandas as pd
from collections import Counter
import csv
import numpy as np
from matplotlib import pyplot as plt

def main():
    """code-here ----------------------------------"""
    """ -----------------------------
       How to check styles available:
       styles = plt.style.available
       print(styles):
       Below after choosing from
       available, implement the style
    ------------------------------"""
    plt.style.use("dark_background")
    # ------------------------------------------------------------
    # ------- Manual way to import a csv ------------------------
    # with open("Languagedata.csv") as csv_file:
    #     # Reads the value by key
    #     csv_reader = csv.DictReader(csv_file)
    #     # Initializing a counter instance
    #     lang_counter = Counter()
    #     # Loops through every row in our cvs file
    #     # In order of the most popular languages
    #     for row in csv_reader:
    #         lang_counter\
    #             .update(row["LanguagesWorkedWith"].split(";"))
    # -----------------------------------------------------------
    # Using a pandas method to import csv
    data = pd.read_csv("Languagedata.csv")
    # ids = data["Responder_id"]
    lang_responses = data["LanguagesWorkedWith"]

    lang_counter = Counter()

    for response in lang_responses:
        lang_counter.update(response.split(";"))
            
    languages = []
    popularity = []
    
    lang_most_common = lang_counter.most_common(20)
    # Prints out the top 15 languages, in a
    # list[()] with tuples of languages inside.
    for item in lang_most_common:
        # Top 15 languages stored.
        languages.append(item[0])
        # How many people voted on the top 15
        popularity.append(item[1])
        
    # To see the top languages from most used to least used.
    languages.reverse()
    popularity.reverse()
    
    # """@test"""
    # formatObjs(languages, popularity)
    
    plt.barh(languages, popularity, color = "#2196f3", label = "Top-Langs")
    
    plt.title("20 Top Programming Languages")
    plt.xlabel("Number Of People Who Use Languages")
    # plt.ylabel("Programming Languages")
    # plt.grid()
    plt.tight_layout()
    
    plt.savefig("HBarChart.png")
    plt.show()
    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
