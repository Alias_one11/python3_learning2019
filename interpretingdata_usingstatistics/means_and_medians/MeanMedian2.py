# import numpy as np
from typing import List

from pandas import Series
import pandas as pd
import matplotlib.pyplot as plt

from python3Learning.src.interpretingdata_usingstatistics \
    .means_and_medians.MeanMedian import height_weight_data
from python3Learning.src.oopBasics.MyFuncs import formatObjs

def main():
    # Panda Series list with outlier values
    list_of_series: List[Series] = [
            pd.Series(["Male", 205, 460], index = height_weight_data.columns),
            pd.Series(["Female", 202, 390], index = height_weight_data.columns),
            pd.Series(["Female", 199, 410], index = height_weight_data.columns),
            pd.Series(["Male", 205, 460], index = height_weight_data.columns),
            pd.Series(["Female", 199, 410], index = height_weight_data.columns),
            pd.Series(["Male", 200, 490], index = height_weight_data.columns)
            ]
    
    # Appending values to the original height Languagedata.csv
    height_weight_updated = height_weight_data.append(list_of_series, ignore_index = True)
    hwu_tail = height_weight_updated.tail()
    formatObjs("Appending values to our weight Languagedata.csv", f"\n{hwu_tail}")


if __name__ == '__main__':
    main()
