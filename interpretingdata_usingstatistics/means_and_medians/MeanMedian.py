# import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from python3Learning.src.oopBasics \
    .MyFuncs import formatObjs, section

height_weight_data = pd.read_csv("500_Person.csv")
hwd_head = height_weight_data.head(5)
print(hwd_head)

# Dropping a column from our Languagedata.csv frame
height_weight_data.drop("Index", inplace = True, axis = 1)
hwd_shape = height_weight_data.shape
formatObjs("Dropped a column:--> Index", hwd_shape)

# Gives you a sum of missing values
hwd_null_sum = height_weight_data.isnull().sum()
formatObjs("Sum of null Languagedata.csv", f"\n{hwd_null_sum}")

# Invoking a specific column for Languagedata.csv
section("Height-Column")
min_height = height_weight_data["Height"].min()
formatObjs("Minimum Height", min_height)
max_height = height_weight_data["Height"].max()
formatObjs("Maximum Height", max_height)
range_of_height = max_height - min_height
formatObjs("Range of height", f"{range_of_height} CM")

section("Weight-Column")
min_weight = height_weight_data["Weight"].min()
formatObjs("Minimum Weight", min_weight)
max_weight = height_weight_data["Weight"].max()
formatObjs("Maximum Weight", max_weight)

# Storing all of the weight in our Languagedata.csv-set
weight = height_weight_data["Weight"]
w_head = weight.head()
formatObjs("\nAll of the Weights in my Data-Set",
           f"\n{w_head}")

# Sorting all of the weights Avg
sorted_weights = weight.sort_values().reset_index(drop = True)
sw_head = sorted_weights.head()
formatObjs("\nSorted weights Avg", f"\n{sw_head}")

# Manually sorting all of the Languagedata.csv
def allValAvgMean(data):
    num_elements = len(data)
    formatObjs("Number of Elements", num_elements)
    
    weight_sum = data.sum()
    formatObjs("Sum:", weight_sum)
    
    result = weight_sum / num_elements
    formatObjs("weight_sum / num_elements", result)
    
    return result

# This function will not work if passed
# un-sorted values
def medianValAvg(data):
    num_elements = len(data)
    
    if num_elements % 2 == 0:
        new_data = (data[(num_elements / 2) - 1]
                    + data[num_elements / 2]) / 2
        formatObjs("Median Value Avg", new_data)
    else:
        new_data = data[((num_elements + 1) / 2) - 1]
        formatObjs("Median Value Avg", new_data)


# Invoke your functions
allValAvgMean(height_weight_data["Weight"])
# Pandas has a function to get the mean
weight_mean = height_weight_data["Weight"].mean()
formatObjs("Panda function to get the mean", weight_mean)

# Invoke your functions. The output
# will be wrong if not sorted
medianValAvg(height_weight_data["Weight"])  # Wrong output..!!NOT SORTED
medianValAvg(sorted_weights)  # Correct Output

# Visualizing the Languagedata.csv with a histogram
plt.figure(figsize = (12, 8))
height_weight_data["Weight"].hist(bins = 30)

plt.axvline(weight_mean, color = 'r', label = "mean")  # vertical line
plt.legend()

# plt.show()
