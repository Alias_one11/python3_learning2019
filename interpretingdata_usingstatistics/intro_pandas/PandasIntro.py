import pandas as pd
import matplotlib
import numpy as np
import seaborn as sns

from python3Learning.src.oopBasics.MyFuncs import formatObjs

mall_data = pd.read_json("csvjson.json")
md_h = mall_data.head(5)
print(md_h)

avg_income = mall_data["annual_income"].mean()
formatObjs("Annual Income Avg", avg_income * 1000)

# Adding a column to the Languagedata.csv
mall_data["above_avg_income"] = \
    (mall_data["annual_income"] - avg_income) > 0
md_s = mall_data.sample(5)
print(md_s)

# Creates a csv version of the file
new_csv = mall_data.to_csv("dataProcessed.csv", index = False)


