from python3Learning.src.oopBasics.MyFuncs import description, section, modIt, factOreo, toFabs, toTheFloor, getCeil, \
    root_Of_ItAll, powToTheMoon, bodyInTheTrunc, mathKing

def main():
    # ==============================================
    description("User-Input & for-range")
    section("::User-Input Examples::")
    # ==============================================
    name = input("What is your name: ")
    print("Hola", name)

    mathKing()
    
    # ----------------------------------------------------
    section("::User-Input covert miles::")
    # ----------------------------------------------------
    user_miles_input = input("Enter miles to convert to km:==> ")
    print(f"Miles To Convert: [ {user_miles_input} ]")
    
    user_miles_input = float(user_miles_input)
    float_num: float = 1.60934
    convert_to_km = user_miles_input * float_num
    
    print(f"{user_miles_input} miles in kilometers = {round(convert_to_km, 2)}")
# ----------------------------------------------------
    section("::Using-Math-Modules::")
# ----------------------------------------------------
    """ A module is just a bunch of
        pre-written code you can use.
        We will be using the functions
        & modifying them to use a revised
        version of those functions """
    # Return the ceiling of x, the smallest
    # integer greater than or equal to x.
    getCeil(17.4)
    # Return the floor of x, the largest
    # integer less than or equal to x.
    toTheFloor(17.4)
    # Return the absolute value of the float x.
    toFabs(17.4)
    # Factorial = 1 * 2 * 3 * 4
    factOreo(4)
    # Returns remainder of division
    modIt(5, 4)
    # Receives a float and returns an int
    bodyInTheTrunc(4)
    # returns x^y
    powToTheMoon(2, 2)
    # Returns the square root
    root_Of_ItAll(4)


if __name__ == '__main__':
    main()
