from python3Learning.src.oopBasics.MyFuncs \
    import description, section, formatObjs, wordsToAcronym, caesarCipher

def main():
    # ==============================================
    description("All-About-Strings Part#2")
    # ==============================================
    ran_str = "This is an important string"
    
    formatObjs("Count of [is] in ran_str", f"|{ran_str.count('is')}|")
    formatObjs("What index is [string] in", f"|{ran_str.find('string')}|")
    """ -----------------------------------------------------
        the [replace] function might confuse ['an'] with
        [import'an't] int the [ran_str] list. so just add a
        space before [' an'] to distinguish one from the other
        ------------------------------------------------- """
    formatObjs("Replacing words in the string",
               f"|{ran_str.replace(' an', ' a kind of')}|")
    # ------------------------------------------------------------------------
    section("Creating a Acronym console app")
    # ------------------------------------------------------------------------
    """ --------------------------------------------------
        Getting the acronym from the user string input
        ---------------------------------------------- """
    wordsToAcronym()
    
    # ------------------------------------------------------------------------
    section("Creating a Caesar-Cipher console app")
    # ------------------------------------------------------------------------
    """ --------------------------------------------------
        Caesar cipher from the user string input
        ---------------------------------------------- """
    caesarCipher()

    
if __name__ == '__main__':
    main()
