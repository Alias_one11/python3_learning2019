from python3Learning.src.oopBasics.MyFuncs \
    import section, description, formatObjs

def main():
    # ==============================================
    description("All-About-Strings Part#1")
    section("::Strings-Examples::")
    # ==============================================
    sample_str = "This is a test of different str functionality"
    formatObjs("Length Of String", len(sample_str))
    
    first_index = sample_str[0].capitalize()
    last_index = sample_str[-1].capitalize()
    fourth_index = sample_str[0:4].capitalize()
    eighth_index = sample_str[8:].capitalize()
    every_otherChar = sample_str[0:-1:2]
    reverseStr = sample_str[::-1]
    
    formatObjs("First Index Letter", first_index)
    formatObjs("Last Index Letter", last_index)
    formatObjs("From the 0 index, up to the fourth index", fourth_index)
    formatObjs("Starts in the 8th index then ", eighth_index)
    formatObjs("Every other index, in steps of two", every_otherChar)
    formatObjs("Reversing a string", reverseStr)
    # ---------------------------------------------------------------------
    section("Working With Unit Codes-Example")
    # ---------------------------------------------------------------------
    print("A's Unit-Code = ", ord("A"))
    print("65's Unit-Code = ", chr(65))
    # ---------------------------------------------------------------------
    """ a - z 97 - 122 (Conversion With secret_str): 122 - 23 = 99
        A - Z 65 - 90 (Conversion With normal_str): 99 + 23 = 122
    """
    normal_str = input("Enter a string to hide in uppercase unit-code : ")
    # a secret string to present to the user
    secret_str = ""
    
    for char in normal_str:
        secret_str += str(ord(char) - 23)
    formatObjs("Secret Message", secret_str)
    # Emptying normal_str
    normal_str = ""
    
    """ Cycling through the string 2 steps at a time """
    for i in range(0, len(secret_str) - 1, 2):
        char_code = secret_str[i] + secret_str[i + 1]
        normal_str += chr(int(char_code) + 23)
    formatObjs("Original Message", normal_str)
    # ---------------------------------------------------------------------
    rand_str = "            This is important string         "
    formatObjs("Tons of white-space", rand_str)
    #########################
    """ [lstrip()]-[rstrip()] gets rid
        of white-space on the left & right"""
    rand_str = rand_str.lstrip()
    rand_str = rand_str.rstrip()
    formatObjs("White-space gone", rand_str)
    # ---------------------------------------------------------------------
    section("List-Of Strings-Example")
    # ---------------------------------------------------------------------
    a_List = ["Bunch", "of", "random", "words"]
    formatObjs("Turning-List To str Then Separating Each Item", " ".join(a_List))
    
    alist2 = rand_str.split()
    
    # printing out all of the items in our list
    for i in alist2:
        print(i)
    

if __name__ == '__main__':
    main()
