import random

from python3Learning.src.oopBasics.MyFuncs import section, description

def main():
    # ==============================================
    description("User-Input & while-break-continue")
    section("::while Examples::")
    # ==============================================
    # to get a range from 0..50
    rand_num = random.randrange(1, 51)
    inc = 1
    
    while inc != rand_num:
        inc += 1
    print(f"The random value is: {rand_num}")
    # ------------------------------------------------
    section("::break-continue Examples::")
    # ------------------------------------------------
    inc2 = 1
    while inc2 <= 20:
        if inc2 % 2 == 0:
            inc2 += 1
            # skips above line
            continue
        if inc2 == 15:
            break
        print(f"Odd numbers: {inc2}")
        inc2 += 1
        

if __name__ == '__main__':
    main()
