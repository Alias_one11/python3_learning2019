from python3Learning.src.oopBasics.MyFuncs import section, description

def main():
    # ==============================================
    description("User-Input & Math-Functions")
    section("::User-Input Examples::")
    # ==============================================
    money = float(input("How much do you want to invest: "))
    interest_rate = float(input("Interest Rate: "))
    yrs_of_investment = int(input("Enter length of investment: "))

    # High return interest
    compound_interest = .03
    interest_rate *= compound_interest
    
    for cash in range(yrs_of_investment):
        money = money + (money * interest_rate)
        
    print(f"Investment after {yrs_of_investment}"
          f" with a interest rate of {interest_rate:.1f}%: ${money:.2f}")
    
    
if __name__ == '__main__':
    main()
