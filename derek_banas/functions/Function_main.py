from python3Learning.src.derek_banas.functions.MyFuncs2 \
    import mult_divide, sumAll, formatStr2
from python3Learning.src.oopBasics.MyFuncs import description

def main():
    # ==============================================
    description("All-About-Functions Part#1")
    # ==============================================
    """ Destructuring the function"""
    multiply, divide = mult_divide(10, 7)
    formatStr2("10 * 7", multiply)
    formatStr2("10 / 7", divide)

    """Function that sums up all values passed"""
    sumAll(11, 15, 25, 14, 17, 25)
    
    
if __name__ == '__main__':
    main()
