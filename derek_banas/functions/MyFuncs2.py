"""Function vault"""
import random

from python3Learning.src.oopBasics.MyFuncs import formatObjs

def formatStr2(*args: object):
    print(f"|---------------------------------------------------\n"
          f"||: --> [{args = }]\n"
          f"|---------------------------------------------------"
          )
    return args

def section2(msg: str) -> str:
    print(f"|----------------------------------------|\n"
          f"|(|{msg}|)        |\n"
          f"|----------------------------------------|")
    return msg


"""--------------------------------------------------
A function that multiplies & divides in one function
--------------------------------------------------"""
def mult_divide(num_1: float, num_2: float):
    return (num_1 * num_2), round(num_1 / num_2, 2)


"""--------------------------------------------------
A function that adds as many numbers as you want
--------------------------------------------------"""
def sumAll(*args):
    sum_1 = 0
    for i in args:
        sum_1 += i
    formatObjs("Sum of all numbers", sum_1)
    return sum_1


"""--------------------------------------------------
Creates random numbers into a list from 1-9
--------------------------------------------------"""
def randomInt(num: int):
    num_List = []
    for i in range(num):
        num_List.append(random.randrange(1, 9))
    for i in num_List:
        print(i, "(|Random Range 1-9|)::\n", end = "")
    print()
