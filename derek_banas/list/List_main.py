import math
from numpy import zeros, array

from python3Learning.src.derek_banas.functions.MyFuncs2 \
    import randomInt
from python3Learning.src.oopBasics.MyFuncs \
    import description, formatObjs

def main():
    # ==============================================
    description("All-About-List Part#1")
    # ==============================================
    """Different types of list"""
    rand_list = ["string", 1.234, 28]
    one_to_ten = list(range(11))
    
    """Adding two list together & then
       getting the values by index"""
    rand_list += one_to_ten
    print(rand_list[0])
    
    """The length of the list"""
    formatObjs("List length", len(rand_list))
    
    """Slicing the list to get a  set of value"""
    first_3 = rand_list[0:3]
    formatObjs("First three items in the list", first_3)
    
    """Iterating through each item & getting there index"""
    for i in first_3:
        tight_format = "Index:: {} : Item:: {}".format(first_3.index(i), i)
        formatObjs("Iterating through items in the list", tight_format)
    
    formatObjs("[string * 3", first_3[0] * 3)
    formatObjs("Is the [string] in first_3", "string" in first_3)
    formatObjs("Index of [string]", first_3.index("string"))
    formatObjs("How many [string] values in the list", first_3.count("string"))
    
    """Replacing & appending new values to the list"""
    first_3[0] = "Alias111"
    first_3.append("Sami")
    for i in first_3:
        tight_format = "Index:: {} : Item:: {}".format(first_3.index(i), i)
        formatObjs("Iterating through items in the list", tight_format)

    # ------------------------------------------------------------------------
    """Generating a random list"""
    # ------------------------------------------------------------------------
    randomInt(7)

    # ------------------------------------------------------------------------
    """Is Even List list"""
    # ------------------------------------------------------------------------
    even_list = [i * 2 for i in range(10)]
    for k in even_list:
        print(k, "(|Even-List|)::\n", end = "")
    print()
    
    """You Can Store Functions Inside Of A List"""
    num_list = [1, 2, 3, 4, 5]
    list_of_values = [[
            math.pow(m, 2), math.pow(m, 3),
            math.pow(m, 4)] for m in num_list
            ]
    for k in list_of_values:
        print(k)
    print()
    
    """Multi-Dimensional List"""
    multi_d_list = []
    for i in range(3):
        multi_d_list.append([0] * 3)
    
    for i in range(3):
        for j in range(3):
            multi_d_list[i][j] = "{} : {}".format(i, j)
            
    for i in range(3):
        for j in range(3):
            print("Multi-D", multi_d_list[i][j], end = " || ")
        print()
    
    """Using numpy zeros function"""
    a = zeros([2, 4], int)
    print(f"\nA 2D array of type int:\n{a}")
    
    """Using numpy [array] function to Create a 2d array"""
    b = array([[5.1, 4.2, 3.3], [4.4, 2.5, 1.6]], float)
    print(f"\nA 2D array of type float:\n{b}")
    
        
if __name__ == '__main__':
    main()
