from typing import List

from python3Learning.src.derek_banas.functions.MyFuncs2 import section2
from python3Learning.src.oopBasics.MyFuncs import description, formatObjs

def main():
    # ==============================================
    description("All-About-List Part#2")
    # ==============================================
    courses_list = ["History", "Math", "Physic", "CompSci"]
    formatObjs("Whole Course list", courses_list)
    formatObjs("First item in Course list at index[0]", courses_list[0])
    formatObjs("Last item in Course list at index[-1]", courses_list[-1])
    formatObjs("Items 0 to 1 before 2 in\nCourse list at index[0:2]",
               courses_list[0:2])
    formatObjs("From the 2nd index[2:] until the end", courses_list[2:])
    
    # ---------------------------------------------------------------------
    # ---------------------------------------------------------------------
    
    """@summary Adding items to a list"""
    
    # Using append(object​)
    courses_list.append('Art')
    # @param def formatObjs(obj_T: object, obj_R: object) -> tuple
    formatObjs(f"Appending a item to our list, at the end", courses_list)
    
    # Using insert(​index, object​)
    courses_list.insert(0, "Music-Theory")
    formatObjs(f"Inserting an item to our list at index[0]", courses_list)
    
    courses_list2 = ["Shop", "Education"]
    # @param def extend(self, iterable: Iterable[_T]) -> None
    courses_list.extend(courses_list2)
    formatObjs(f"Extends function, to add courses 1 to course 2", courses_list)

    # ---------------------------------------------------------------------
    """@param def section2(msg: str) -> str"""
    section2("Removing items from the list")
    # ---------------------------------------------------------------------
    courses3: List[str] = ["Java", "UML-Diagram", "Calc1", "Discrete-Math", "Eng-Theory"]
    formatObjs("All original courses shown", courses3)

    courses3.remove("Eng-Theory")
    formatObjs("Removing at index[-1]", courses3)
    
    # Popped the last item in the list
    # def pop(self, index: int = ...) -> _T
    popped: str = courses3.pop()
    formatObjs("Printing out popped course", popped)
    formatObjs("Courses remaining", courses3)
    
    courses3.extend(courses_list)
    formatObjs("Combined list of courses", courses3)
    
    """@summary off loading some course"""
    courses3.pop()
    courses3.pop()
    courses3.pop()
    courses3.pop()
    courses3.pop()
    courses3.pop()
    
    formatObjs("Remaining courses after pops", courses3)
    
    # Reversing our list
    # def reverse(self) -> None
    courses3.reverse()
    formatObjs("Our list in reverse", courses3)
    
    """--------------------------------------------
       @sort\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
       sorting our list in alphabetical order\\\\\\
       [sort(​​)], [sort(​key​)], [sort(​key, reverse​)]\
       [sort(​cmp​)], [sort(​reverse​)]\\\\\\\\\\\\\\\\
       -----------------------------------------"""
    list_of_nums = [102, 3, 4, 67, 9, 78, 6, 1, 99, 12]
    list_of_nums.sort()
    formatObjs("Our list of numbers sorted from low to high", list_of_nums)
    
    """@sort: IN DESCENDING ORDER"""
    list_of_nums.sort(reverse = True)
    formatObjs("Our list of numbers sorted from high to low", list_of_nums)

    
if __name__ == '__main__':
    main()
