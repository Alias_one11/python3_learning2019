import sys

from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox

class MsgBoxApp(QWidget):
    def __init__(self):
        super().__init__()
        
        """@code in here--------------------------------------------------"""
        
        self.InitUI()
        """@code in here--------------------------------------------------"""
    
    """@function in here--------------------------------------"""
    
    def InitUI(self):
        ans = QMessageBox.question(
                self, "Beverage", "Do you like coffee",
                QMessageBox.Help | QMessageBox.Yes |
                QMessageBox.No, QMessageBox.Yes
                )
        if ans == QMessageBox.Yes:
            print("Yes")
        elif ans == QMessageBox.No:
            print("No")
        
        self.show()
    
    """@function in here--------------------------------------"""


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MsgBoxApp()
    sys.exit(app.exec())
