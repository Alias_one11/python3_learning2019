from tkinter import *
from tkinter import ttk

root = Tk()
btn = ttk.Button(root, text = "Click Me")
# Add the geometry manager
btn.pack()

def onclicked():
    print("Button Clicked!!!")


btn.config(command = onclicked)


root.mainloop()
