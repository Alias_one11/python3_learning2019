import sys
from PyQt5.QtWidgets import QMainWindow, QApplication

class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        """@code in here--------------------------------------------------"""
        self.title = "PyQt5 Window"
        self.top = 100
        self.left = 100
        self.width = 400
        self.height = 300
        self.statusMsg = "Bar Hoping"
        
        # Calling our method
        self.initUI()
        
        """@code in here--------------------------------------------------"""

    """@function in here--------------------------------------"""
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage(self.statusMsg)
        self.show()

    """@function in here--------------------------------------"""


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec())
