import sys
from typing import Tuple

from PyQt5.QtWidgets import QDialog, QApplication, QInputDialog

class InputDialogApp(QDialog):
    
    def __init__(self):
        super().__init__()

        """@code in here--------------------------------------------------"""
        self.title = "Input Dialog"
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 400
        
        self.InitUI()
        
        """@code in here--------------------------------------------------"""

    """@function in here--------------------------------------"""

    def InitUI(self):
        title: str = self.title
        left: int = self.left
        top: int = self.top
        width: int = self.width
        height: int = self.height

        self.setWindowTitle(title)
        self.setGeometry(left, top, width, height)
        
        color = self.pickColor
        print(color())
        
        # self.show()
        
    def pickColor(self):
        items: Tuple[str, str, str] = ("Red", "Blue", "Green")
        getIt: str = "Get item"
        color: str = "Color"
        
        item, ok_pressed = QInputDialog.getItem(self, getIt, color, items, 0, False)
        if ok_pressed and item:
            print(item)
            
    """@function in here--------------------------------------"""


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = InputDialogApp()
    sys.exit(app.exec())
