import wx

class MyForm(wx.Frame):
    
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Background Change Tutorial")
        """@code in here--------------------------------------------------"""
        self.panel = wx.Panel(self, wx.ID_ANY)
        self.txt = wx.TextCtrl(self.panel)
        self.txt.SetBackgroundColour("Yellow")
        
        blueBtn = wx.Button(self.panel, label = "Change BackGround Color")
        blueBtn.Bind(wx.EVT_BUTTON, self.onChgBackground)
        
        resetBtn = wx.Button(self.panel, label = "Reset")
        resetBtn.Bind(wx.EVT_BUTTON, self.onReset)
        
        topSizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        
        btnSizer.Add(blueBtn, 0, wx.ALL | wx.CENTER, 5)
        btnSizer.Add(resetBtn, 0, wx.ALL | wx.CENTER, 5)
        topSizer.Add(self.txt, 0, wx.ALL, 5)
        topSizer.Add(btnSizer, 0, wx.CENTER)
        # def SetSizer(self, sizer: Any, deleteOld: bool = True)
        self.panel.SetSizer(topSizer)
        
        """@code in here--------------------------------------------------"""
    
    """@function in here--------------------------------------"""
    
    def onChgBackground(self, event):
        # def SetBackgroundColour(self, colour: Any)
        self.panel.SetBackgroundColour("Black")
        # def Refresh(self, eraseBackground: bool = True, rect: Any = None)
        self.panel.Refresh()
        return event
    
    def onReset(self, event):
        self.panel.SetBackgroundColour(wx.NullColour)
        self.txt.SetBackgroundColour(wx.NullColour)
        self.panel.Refresh()
        
        return event
    
    """@function in here--------------------------------------"""
    

if __name__ == '__main__':
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()
