class Employee:
    numOfNewEmp = 0
    raiseAmt = 1.04
    payIncrease: float
    
    # Constructor
    def __init__(self, fName, lName, pay):
        self.fName = fName
        self.lName = lName
        self.pay = pay
        self.email = f"{self.fName}_{self.lName}@company.com"
        # Keeping count of new employees
        Employee.numOfNewEmp += 1
        
    # Secondary Constructor that parses string
    @classmethod
    def fromStr(cls, emptyStr):
        fName, lName, pay = emptyStr.split('-')
        # The same as: Employee(fName, lName, pay)
        return cls(fName, lName, pay)
    
    def empInfo(self):
        return f"First Name:[ {self.fName} ]\n" \
               f"Last Name:[ {self.lName} ]\n" \
               f"Salary:[ ${self.pay:.2f} ]\n" \
               f"Email:[ {self.email} ]\n"
    
    def applyRaise(self):
        self.payIncrease = float(self.pay * self.raiseAmt)
        print(f"Employee: [ {self.fName} {self.lName}]\n"
              f"Pay Increase: [ ${self.payIncrease:.2f} ]\n")
    
    # This method will receive the class
    # first instead of the instance
    @classmethod
    def set_raiseAmt(cls, amount):
        cls.raiseAmt = amount
        
    """Use of static methods are useful in any
       programming language where you are not
       using any members of the class"""
    @staticmethod
    def a_workDay(day):
        # weekday() method is from the date in datetime library
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True
