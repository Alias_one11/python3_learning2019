class EmployeeProp:
    
    def __init__(self, fName, lName):
        self.fName = fName
        self.lName = lName
    
    # The getter
    @property
    def email(self):
        return f"\nEmail: [ {self.fName}.{self.lName}@DevTech.com ]"
    
    # The getter
    @property
    def emp_fullname(self):
        return f"Full Name: [ {self.fName} {self.lName} ]\n"
    
    # Creating a setter
    @emp_fullname.setter
    def emp_fullname(self, name):
        fnName, lName = name.split(" ")
        self.fName = fnName
        self.lName = lName
        
    # Delete's the full name
    @emp_fullname.deleter
    def emp_fullname(self):
        print("[[ Employee..[DELETED]!!! ]]")
        self.fName = None
        self.lName = None
