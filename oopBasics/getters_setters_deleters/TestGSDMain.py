from python3Learning.src.oopBasics.MyFuncs import section, description
from python3Learning.src.oopBasics.getters_setters_deleters.ClsEmployeeProp import EmployeeProp

def main():
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Getters|Setters|Deleters-Example#1")
    # =====================================================
    description("Testing our @property's")
    
    emp1 = EmployeeProp("Alias", "Sam")
    # Original first name
    print(f"Original first name: [ {emp1.fName} ]")

    # Changing the employees first name
    emp1.fName = "Larry"
    # Printing the first name with its new name
    print(f" New first name: [ {emp1.fName} ]")
    
    # Using our email property
    print(emp1.email)
    # Using our full name property
    print(emp1.emp_fullname)
    # =====================================================
    description("Testing  @emp_fullname.setter")
    
    emp1.emp_fullname = "Samuel Martin"
    
    print(f"(|Employees new full name|):\n{emp1.emp_fullname}")
    print(f"(|New full name email address, changed state|):"
          f"{emp1.email}")
    
    print()
    # =====================================================
    description("Testing @emp_fullname.deleter")
    
    # deleting the full name we created
    del emp1.emp_fullname
    # The full name should print out empty|| None None
    print(emp1.emp_fullname)

    
if __name__ == '__main__':
    main()
