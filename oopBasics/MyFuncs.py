import math

def section(msg):
    print("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(f"(|{msg}|)")
    print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

def description(msg):
    print("=====================================================")
    print(f"[|{msg}|]")
    print("=====================================================")

def formatObjs(obj_T: object, obj_R: object) -> tuple:
    print(f"|---------------------------------------------------\n"
          f"|(| {obj_T} |)\n|: --> {obj_R}\n"
          f"|---------------------------------------------------"
          )
    return obj_T, obj_R


""" --------------------------------------------------
    Getting the acronym from the user string input
    ---------------------------------------------- """
def wordsToAcronym():
    orig_str = ""
    quit_it = "quit".upper()
    
    while orig_str != quit_it:
        orig_str = input("Enter string to convert to ACRONYM: ")
        if orig_str == quit_it:
            break
        else:
            orig_str = orig_str.upper()
            # Converting the string to a list[]
            list_into_acronym = orig_str.split()
            """ -------------------------------------
                Cycling through the list, to get the
                first letter of each of the words
                --------------------------------- """
            for word in list_into_acronym:
                """ -------------------------------------
                    word[0] gets the first letter of each
                    individual word & end = "" separates
                    the new lines
                    --------------------------------- """
                print(word[0], end = "")
            print()


""" --------------------------------------------------
    Caesar cipher from the user string input
    ---------------------------------------------- """

def caesarCipher():
    msg = ""
    quit_it = 0
    key: int
    
    while msg != quit_it:
        key = int(input("[0 to quit]: How many characters should we shift (1 - 26) : "))
        if key == quit_it:
            break
        msg = input("Enter message : ")
        secret_msg = ""
        if msg == quit_it:
            break
        else:
            for char in msg:
                """ -------------------------
                    if it is not a letter, i
                    do not want to change it
                    ------------------------- """
                if char.isalpha():
                    char_code = ord(char)
                    char_code += key
                    """ ----------------------------------------
                        [ord()] function: Return the Unicode
                         code point for a one-character string.
                        ---------------------------------------- """
                    if char.isupper():
                        if char_code > ord('Z'):
                            char_code -= 26
                        elif char_code < ord('A'):
                            char_code += 26
                    else:
                        if char_code > ord('z'):
                            char_code -= 26
                        elif char_code < ord('a'):
                            char_code += 26
                    
                    """ ------------------------------------------------
                       [chr()] function: Return a Unicode string of one
                        character with ordinal i; 0 <= i <= 0x10ffff
                        or converting the code back to char letter.
                       --------------------------------------------- """
                    secret_msg += chr(char_code)
                else:
                    """|-------------------------------------------
                       |if its not a letter then return char
                       |for what ever that character is
                       |----------------------------------------"""
                    secret_msg += char
            formatObjs("Original User Message", msg)
            formatObjs("Message Encrypted", secret_msg)
            
            print("::Check the cipher is correct::")
            print("---------------------------\n"
                  " A B C D E F G H I J K L M\n"
                  " N O P Q R S T U V W X Y Z\n"
                  "---------------------------\n")
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
def mathKing():
    # To handle the user input once the "operator" string: +,-,*,/,%
    # is added to the argument of the function call.
    # With the two numbers the user inputted
    add, div, mod, num_1, num_2, sub, times = numUserInput()
    operator = input("Choose a operator[+, -, *, /, %]: ")
    if operator == '+':
        print(f"---------------------------------\n"
              f"{num_1} + {num_2} = {add}\n"
              f"---------------------------------")
    elif operator == '-':
        print(f"---------------------------------\n"
              f"{num_1} - {num_2} = {sub}\n"
              f"---------------------------------")
    elif operator == '*':
        print(f"---------------------------------\n"
              f"{num_1} * {num_2} = {times}\n"
              f"---------------------------------")
    elif operator == '/':
        print(f"---------------------------------\n"
              f"{num_1} / {num_2} = {round(div, 2)}\n"
              f"---------------------------------")
    elif operator == '%':
        print(f"---------------------------------\n"
              f"{num_1} % {num_2} = {mod}\n"
              f"---------------------------------")
    return num_1, num_2

# -------------------------------------------------------------------------

def numUserInput():
    num_1, num_2 = input("Enter 2 numbers: ").split()
    """Casting num_1 & num_2 to [float]"""
    num_1 = float(num_1)
    num_2 = float(num_2)
    add: float = num_1 + num_2
    sub: float = num_1 - num_2
    times: float = num_1 * num_2
    div: float = num_1 / num_2
    mod: float = num_1 % num_2
    return add, div, mod, num_1, num_2, sub, times

# ============================================================================
# ============================================================================

def modIt(num1: int, num2: int):
    print(f"---------------------------------\n"
          f"fmod({num1, num2}) = {math.fmod(5, 4)}\n"
          f"---------------------------------")

def factOreo(num: int):
    print(f"---------------------------------\n"
          f"factorial({num}) = {math.factorial(num)}\n"
          f"---------------------------------")

def toFabs(num: float):
    print(f"fabs({num}) = {math.fabs(num)}\n"
          f"---------------------------------")

def toTheFloor(num: float):
    print(f"floor({num}) = {math.floor(num)}")

def getCeil(num: float):
    print(f"---------------------------------\n"
          f"ceil({num}) = {math.ceil(num)}")

def root_Of_ItAll(num: int) -> int:
    result = math.sqrt(num)
    print(f"---------------------------------\n"
          f"sqrt({num}) = {result}\n"
          f"---------------------------------")
    return int(result)

def powToTheMoon(num_1: int, num_2: int) -> int:
    result = math.pow(num_1, num_2)
    print(f"---------------------------------\n"
          f"pow({num_1, num_2}) = {result}\n"
          f"---------------------------------")
    return int(result)

def bodyInTheTrunc(num: int):
    result = math.trunc(num)
    print(f"---------------------------------\n"
          f"trunc({num}) = {result}\n"
          f"---------------------------------")
    return int(result)

# ============================================================================
def guessingGame(secret_num: int):
    while True:
        guess = int(input("Guess a number from 1 to 20: "))
        
        if guess != secret_num:
            print("!!![WRONG]!!!..Try again")
        elif guess == secret_num:
            print("You guessed it!!!")
            break
