# Organizes the Console Output
import datetime

from python3Learning.src.oopBasics.ClsEmployee import Employee
from python3Learning.src.oopBasics.MyFuncs import *

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def main():
    
    """[145_567.4571]..etc Testing that it
    is rounding to the nearest 2 place"""
    section("Employee Example#1")
    emp1 = Employee('Alias111', "The-Great-One", 167_768.1467)
    
    print(emp1.empInfo())
    emp1.applyRaise()
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Employee Example#2")
    emp2 = Employee('Jason', "Tran", 145_567.4571)
    
    print(emp2.empInfo())
    emp2.applyRaise()
    
    # Seeing how many Employees
    # instances were created
    howMany = Employee.numOfNewEmp
    print(f"Number Of Employee Instances: [ {howMany} ]")
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Employee Example#3")
    description("Working with class methods & Static methods")
    
    print(emp2.empInfo())
    
    emp2.set_raiseAmt(1.10)
    emp2.applyRaise()

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Employee Example#4")
    description("Parsing string with class method")
    
    empStr1 = "John-Godie-70000"
    # Parsing the string to create
    # an instance of employee by passing
    # the employee string
    emp3 = Employee.fromStr(empStr1)
    
    print(f"First Name: [ {emp3.fName} ]")
    print(f"Last Name: [ {emp3.lName} ]")
    print(f"Salary: [ {emp3.pay} ]")
    print(f"Email: [ {emp3.email} ]")

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Employee Example#4")
    description("Static-Method & Using datetime lib")
    
    myDate = datetime.date(2019, 12, 15)
    workDay = Employee.a_workDay(myDate)
    print(f"Is it a work day today? [ {workDay} ]")
    
    
if __name__ == '__main__':
    main()
