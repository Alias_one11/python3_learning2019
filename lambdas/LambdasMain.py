"""
A regular function in python
def cube(n):
    return n**3
makes what ever number passed [cubed]
"""
from python3Learning.src.lambdas.MylambdaFuncs import *
from python3Learning.src.oopBasics.MyFuncs import *

def main():
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Lambdas-Example#1")
    
    description("Lambda Functions: iceCube(), isEven_isOdd()")
    iceCube(2)
    isEven_isOdd(234567)
    
    print()
    
    description("Lambda Functions: add(),sub(),\ntimes(),div(),mod()")
    add(10, 7)
    sub(10, 7)
    times(10, 7)
    div(10, 7)
    mod(10, 7)

    print()
    
    description("Lambda Functions: filter_aList(),map_times2()\n,reduce_thatList()")
    filter_aList()
    map_times2()
    reduce_thatList()


if __name__ == '__main__':
    main()
