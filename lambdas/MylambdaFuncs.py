from functools import reduce

def iceCube(num):
    # lambda
    lamCube = lambda n: n ** 3
    result = f"(|Number [ {num} ] Cubed|): [ {lamCube(num)} ] <--"
    return print(result)

def isEven_isOdd(num):
    # lambda
    bettaBe = lambda x: "Even" if x % 2 == 0 else "Odd"
    result = f"(|Is [ {num} ] Even/Odd?|): [ {bettaBe(num)} ] <--"
    return print(result)

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def add(a, b):
    # lambda
    result = lambda x, y: x + y
    getIt = f">{a} + {b} = [ {result(a, b)} ] <--"
    return print(getIt)

def sub(a, b):
    # lambda
    result = lambda x, y: x - y
    getIt = f">{a} - {b} = [ {result(a, b)} ] <--"
    return print(getIt)

def times(a, b):
    # lambda
    result = lambda x, y: x * y
    getIt = f">{a} * {b} = [ {result(a, b)} ] <--"
    return print(getIt)

def div(a, b):
    # lambda
    result = lambda x, y: x / y
    getIt = f">{a} / {b} = [ {round(result(a, b), 2)} ] <--"
    return print(getIt)

def mod(a, b):
    # lambda
    result = lambda x, y: x % y
    getIt = f">{a} % {b} = [ {result(a, b)} ] <--"
    return print(getIt)
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

def filter_aList():
    aList = [12, 45, 34, 106, 17, 5]
    # lambda
    filter_it = list(filter(lambda x: x % 2 == 0, aList))
    result = f"(|The list filtered even numbers|): [ {filter_it} ] <--"
    return print(result)

def map_times2():
    aMap_list = [2, 3, 4, 5]
    # lambda
    mapIt = list(map(lambda n: n * 2, aMap_list))
    getIt = f"(|My map times * 2|): [ {mapIt} ] <--"
    return print(getIt)

def reduce_thatList():
    aReduce_list = [5, 10, 15, 20]
    result = reduce(lambda x, y: x + y, aReduce_list)
    getIt = f"(|Returns the sum of all the numbers|): [ {result} ]"
    return print(getIt)