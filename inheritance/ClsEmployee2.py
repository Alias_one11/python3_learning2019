class Employee2:
    raiseAmt = 1.04
    
    # Constructor
    def __init__(self, fName, lName, pay):
        self.fName = fName
        self.lName = lName
        self.pay = pay
        self.email = f"{self.fName}_{self.lName}@company.com"
    
    def empInfo2(self):
        return f"First Name:[ {self.emp_fullname()} ] <-\n" \
               f"Salary:[ ${self.pay:.2f} ] <-\n" \
               f"Email:[ {self.email} ] <-\n"
    
    def applyRaise2(self):
        self.pay = float(self.pay * self.raiseAmt)
        print(f"\nEmployee: [ {self.fName} {self.lName}]\n"
              f"Pay Increase: [ ${self.pay:.2f} ]\n")
    
    def emp_fullname(self):
        print(f"Full Name: [ {self.fName} {self.lName} ]")
