# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
from python3Learning.src.inheritance.Developer import Developer
from python3Learning.src.inheritance.ClsEmployee2 import Employee2
from python3Learning.src.inheritance.Manager import Manager
from python3Learning.src.oopBasics.MyFuncs import section, description

def main():
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Inheritance-Example#1")
    
    dev1 = Developer("Alias111", "El-Rey", 80_000, "Kotlin")
    # print(help(Developer))
    
    print(dev1.empInfo2())
    dev1.applyRaise2()
    print(f"\nDeveloper Salary:=> [ ${dev1.pay} ]\n")

    print(dev1.empInfo2())
 
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Inheritance-Example#2")
    
    """Changing the raise amount in our
       child class developer did not change
       that amount of "raiseAmt = 1.04" or 4%
       in the parent class [Employee]. It remained
       the same while in the child class it is
       now at "raiseAmt = 1.10" or 10% """
    dev2 = Employee2("Cory", "Schafer", 70_000)

    print(dev2.empInfo2())
    dev2.applyRaise2()
    
    print(f"\nDeveloper Salary:=> [ ${dev2.pay} ]\n")
    print(dev2.empInfo2())

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Inheritance-Example#2")
    description("Using our manager class")
    
    mgr1 = Manager("Charles", "Xavier", 133_000, [dev1, dev2])
    
    # Checking how inheritance & all
    # the extra functionality the "Manager"
    # class has, compared to the other child classes
    print(mgr1.email)
    print()
    # Printing all the employees of the manager
    mgr1.printEmps()
    # Removing an employee
    mgr1.removeEmp(dev2)
    mgr1.printEmps()

    
if __name__ == '__main__':
    main()
