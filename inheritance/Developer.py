from python3Learning.src.inheritance.ClsEmployee2 import Employee2

class Developer(Employee2):
    # [overriding] the parent class
    # member "raiseAmt"
    raiseAmt = 1.10
    
    # Constructor
    def __init__(self, fName, lName, pay, prog_lang):
        super().__init__(fName, lName, pay)
        self.prog_lang = prog_lang
        
    # Overriding the "empInfo2()" to be
    # able to also see what programming
    # language the developer works with.
    def empInfo2(self):
        return f"{super().empInfo2()}" \
               f"Programming Language: [ {self.prog_lang} ] <-"
