from python3Learning.src.inheritance.ClsEmployee2 import *

class Manager(Employee2):
    # Constructor
    def __init__(self, fName, lName, pay, employees = None):
        super().__init__(fName, lName, pay)
        if employees is None:
            self.employees = []
        else:
            self.employees = employees
    
    # Adds employees to our list
    def addEmp(self, emp):
        if emp not in self.employees:
            # append adds an employee
            self.employees.append(emp)
    
    # Removes employees to our list
    def removeEmp(self, emp):
        if emp in self.employees:
            # remove's an employee
            self.employees.remove(emp)
            
    # Iterates through all the employees
    # & prints there full name's
    def printEmps(self):
        for emp in self.employees:
            print(emp.emp_fullname())
