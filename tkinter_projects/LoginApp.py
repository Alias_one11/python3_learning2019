import tkinter as tk

class LoginApp(tk.Tk):
    
    def __init__(self):
        super().__init__()
        
        """@code in here--------------------------------------------------"""
        # tkinter.Entry(master = None, cnf = builtins.dict, **kw)
        self.username = tk.Entry(self)
        # will display each typed character as an asterisk
        self.pw = tk.Entry(master = self, show = "*")
        self.login_btn = tk.Button(self, text = "Login", command = self.printLogin)
        self.clear_btn = tk.Button(self, text = "Clear", command = self.clearForm)
        
        # pack() it up
        self.username.pack()
        self.pw.pack()
        self.login_btn.pack()
        self.clear_btn.pack()
        """@code in here--------------------------------------------------"""
    # -----------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------
    """@function in here--------------------------------------"""
    
    def printLogin(self) -> [str, str]:
        # With the get() method, we will
        # retrieve the current text as a string.
        user = f"User Name: {self.username.get()}"
        password = f"Password: {self.pw.get()}"
        print(user)
        print(password)
        
        return user, password
    
    def clearForm(self):
        """@summary: delete(​first, last​)------------------------------------------
           The delete() method takes two arguments that indicate the range of
           the characters that should be deleted. Keep in mind that the indices
           start at the position 0, and they do not include the character at the
           end of the range. If only one argument is passed, it deletes the
           character at that position
           --------------------------------------------------------------------"""
        self.username.delete(first = 0, last = tk.END)
        self.pw.delete(first = 0, last = tk.END)
        
        """@summary-------------------------------------------------
           Direct input focus to this widget. If the application
           currently does not have the focus this widget will get
           the focus if the application gets the focus through
           the window manager.
        ---------------------------------------------------------"""
        self.username.focus_set()
    
    """@function in here--------------------------------------"""


if __name__ == '__main__':
    app = LoginApp()
    app.mainloop()
