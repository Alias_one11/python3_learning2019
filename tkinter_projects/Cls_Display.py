import tkinter as tk

class Display:
    def __init__(self):
        """@summary -----------------------------------------------------------------
        tkinter.Tk ( screenName=None, baseName=None, className='Tk', useTk=1 )
        The [Tk] class is instantiated [without arguments]. This creates a toplevel
         [widget] of [Tk] which usually is the [main window] of an [application]. Each
         instance has its own associated [Tcl] interpreter.
         --------------------------------------------------------------------------"""
        self.root = tk.Tk()
        """@code in here--------------------------------------------------"""
        self.root.title("Alias_one11-TKinter")
        
        # Create Widgets
        self.btn1 = tk.Button(self.root, text = "Button-1", command = lambda: self.onclick(1))
        self.btn2 = tk.Button(self.root, text = "Button-2", command = lambda: self.onclick(2))
        
        # To Show The Buttons
        """@summary---------------------------------------------
           pack(cnf=builtins.dict, **kw):
           ==============================
           [HOW OTHERS USED THIS]
           pack(​​)
           pack(​side​)
           pack(​fill, side​)
           pack(​expand, fill​)
           pack(​fill, side, expand​)
        -----------------------------------------------------"""
        self.btn1.pack()
        self.btn2.pack()
        
        """@code in here--------------------------------------------------"""
        self.root.mainloop()
        
    @staticmethod
    def onclick(args: int) -> [str]:
        msg1: str = "Button# [1] Clicked"
        msg2: str = "Button# [2] Clicked"
        
        if args == 1:
            print(msg1)
        elif args == 2:
            print(msg2)
            

display = Display()
