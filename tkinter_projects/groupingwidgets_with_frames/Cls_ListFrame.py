import tkinter as tk

"""@attention:---------------------------------------------------------------
   Our [ListFrame] class has only two methods to interact with the inner
   list: [popSelect()] and [insertItem()]:
   =========================================================
   popSelect(): returns and deletes the current selection,
   or none if there is no item selected.
   ---------------------------------------------------------
   insertItem(): inserts a new item at the end of the list.
--------------------------------------------------------------------------"""

class ListFrame(tk.Frame):
    def __init__(self, master, items = ()):
        super().__init__(master)
        """@code in here--------------------------------------------------"""
        self.list = tk.Listbox(self)
        self.scroll = tk.Scrollbar(self, orient = tk.VERTICAL,
                                   command = self.list.yview)
        
        self.list.config(yscrollcommand = self.scroll.set)
        self.list.insert(0, *items)
        
        """@summary pack() it up"""
        self.list.pack(side = tk.LEFT)
        self.scroll.pack(side = tk.LEFT, fill = tk.Y)
        
        """@code in here--------------------------------------------------"""

    """@function in here--------------------------------------"""

    def popSelect(self):
        index = self.list.curselection()
        if index:
            value = self.list.get(index)
            self.list.delete(index)
            return value
        
    def insertItem(self, item):
        self.list.insert(tk.END, item)
            
    """@function in here--------------------------------------"""
