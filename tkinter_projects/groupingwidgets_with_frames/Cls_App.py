import tkinter as tk

from python3Learning.src.tkinter_projects.groupingwidgets_with_frames.Cls_ListFrame\
    import ListFrame

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        """@code in here--------------------------------------------------"""
        
        months = [
                "January", "February", "March", "April",
                "May", "June", "July", "August",
                "September", "October", "November", "December"
                ]
        self.frame_a = ListFrame(self, months)
        self.frame_b = ListFrame(self)
        
        self.btn_right = tk.Button(self, text = ">",
                                   command = self.moveRt)
        self.btn_left = tk.Button(self, text = "<",
                                  command = self.moveLt)
        
        """@summary: pack() it up"""
        # parent frame containers to correctly
        # pack them with the appropriate padding
        # Thanks to these frames, our calls to the
        # geometry manager are more isolated and
        # organized in our global layout.
        self.frame_a.pack(side = tk.LEFT, padx = 10, pady = 10)
        self.frame_b.pack(side = tk.RIGHT, padx = 10, pady = 10)
        
        self.btn_left.pack(expand = True, ipadx = 5)
        self.btn_right.pack(expand = True, ipadx = 5)
        
        """@code in here--------------------------------------------------"""
    
    """@function in here--------------------------------------"""
    
    def moveRt(self):
        self.move(self.frame_a, self.frame_b)
    
    def moveLt(self):
        self.move(self.frame_b, self.frame_a)
    
    """@attention:---------------------------------------
       to transfer an item from one list to the other one
    --------------------------------------------------"""
    @staticmethod
    def move(frame_from, frame_to):
        value = frame_from.popSelect()
        if value:
            frame_to.insertItem(value)
    
    """@function in here--------------------------------------"""
    
    
if __name__ == '__main__':
    app = App()
    app.title("List Frame App")
    app.mainloop()
