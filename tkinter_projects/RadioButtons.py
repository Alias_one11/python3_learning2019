import tkinter as tk
from typing import List, Tuple

COLORS: List[Tuple[str, str]] = [
        ("Red", "red"),
        ("Green", "green"),
        ("Blue", "blue")
        ]

class RadioButtonsApp(tk.Tk):

    def __init__(self):
        super().__init__()
        """@code in here--------------------------------------------------"""
        self.var = tk.StringVar()
        self.var.set("red")

        self.btns = [self.createRadioBtn(color) for color in COLORS]
        """Long version below: Above shorthand synthetic sugar"""
        # self.btns = []
        # for color in COLORS:
        #     self.btns.append(self.createRadioBtn(color))
        
        for button in self.btns:
            button.pack(anchor = tk.W, padx = 20, pady= 10)
        """@code in here--------------------------------------------------"""

    """@function in here--------------------------------------"""

    def createRadioBtn(self, option):
        text, value = option
        return tk.Radiobutton(master = self, text = text,
                              value = value,
                              command = self.printOption,
                              variable = self.var)
    
    def printOption(self):
        print(self.var.get())
    
    """@function in here--------------------------------------"""


if __name__ == '__main__':
    app = RadioButtonsApp()
    app.title("Radio-Button")
    app.mainloop()
