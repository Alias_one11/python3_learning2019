import tkinter as tk
from typing import List

DAYS: List[str] = [
        "Monday", "Tuesday",
        "Wednesday", "Thursday",
        "Friday", "Saturday", "Sunday"
        ]
"""@summary----------------------------------------------------------
   str(object='') -> str
   str(bytes_or_buffer[, encoding[, errors]]) -> str
   Create a new string object from the given object. If encoding or
   errors is specified, then the object must expose a Languagedata.csv buffer
   that will be decoded using the given encoding and error handler.
   Otherwise, returns the result of object.__str__() (if defined)
   or repr(object). encoding defaults to sys.getdefaultencoding().
   errors defaults to 'strict'.
   =================================================================
   ([EXAMPLE]: tk.SINGLE, tk.BROWSE, tk.MULTIPLE, tk.EXTENDED)
   [SINGLE]: Single choice
   [BROWSE]: Single choice that can be moved with the up and down
   keys
   [MULTIPLE]: Multiple choice
   [EXTENDED]: Multiple choice with ranges that can be selected with
   the Shift and Ctrl keys
   =================================================================
------------------------------------------------------------------"""
MODES: List[str] = [
        tk.SINGLE, tk.BROWSE,
        tk.MULTIPLE, tk.EXTENDED
        ]

class ListItemsApp(tk.Tk):
    def __init__(self):
        super().__init__()
        """@code in here--------------------------------------------------"""
        self.list = tk.Listbox(master = self)
        # insert(index, *elements)
        self.list.insert(0, *DAYS)
        
        self.printBtn = tk.Button(master = self, text = "Print selection",
                                  command = self.printSel)
        # self.btns = [self.createBtn(mode) for mode in MODES]
        self.btns = []
        for mode in MODES:
            self.btns.append(self.createBtn(mode))
            
        """@group pack() it up"""
        self.list.pack()
        self.printBtn.pack(fill = tk.BOTH)
        for btn in self.btns:
            btn.pack(side = tk.LEFT)
        
        """@code in here--------------------------------------------------"""

    """@function in here--------------------------------------"""
    
    def createBtn(self, mode):
        # config(cnf=None, **kw)
        cmd = lambda: self.list.config(selectmode = mode)
        # Button(master=None, cnf=builtins.dict, **kw)
        return tk.Button(master = self, command = cmd,
                         text = mode.capitalize())
    
    def printSel(self):
        """@attention curselection():
           Return the indices of currently selected item."""
        selection = self.list.curselection()
        print([self.list.get(i) for i in selection])

    """@function in here--------------------------------------"""
    
    
if __name__ == '__main__':
    app = ListItemsApp()
    app.title("List Of Items")
    app.mainloop()
