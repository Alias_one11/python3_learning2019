from tkinter import *
from tkinter import ttk

root = Tk()
label = ttk.Label(root, text = "Hola TKinter!")
label.pack()

label.config(text = "Dimelo, TKinter!")
label.config(justify = CENTER)
label.config(foreground = 'blue', background = 'yellow')
label.config(font = ("Nosifer", 18, "underline"))

root.mainloop()
