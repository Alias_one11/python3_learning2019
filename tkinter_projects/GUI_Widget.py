import tkinter as tk

class Display:
    def __init__(self):
        self.root = tk.Tk()
        """@code in here--------------------------------------------------"""
        self.root.title("GUI-Entry")
        # Entry widget which allows to display simple text.
        self.entry1 = tk.Entry(master = self.root)

        # Binding a key to our entry1
        self.entry1.bind(sequence = "<Return>", func = self.onReturn)
        self.entry1.pack()
        
        """@code in here--------------------------------------------------"""
    
        self.root.mainloop()
        
    def onReturn(self, event: object) -> object:
        msg1: str = "Return [PRESSED]"
        print(msg1)

        value = self.entry1.get()
        print(value)
        # Clears the text on Return [PRESSED]
        self.entry1.delete(first = 0, last = "end")
        
        return msg1, value, event
        
        
display = Display()
