import tkinter as tk

class TextChgApp(tk.Tk):
    def __init__(self):
        super().__init__()
        
        """@code in here--------------------------------------------------"""
        self.var = tk.StringVar()
        """@summary------------------------------------------------
           "w": Called when the variable is written
           "r": Called when the variable is read
           "u" (for unset): Called when the variable is deleted
        --------------------------------------------------------"""
        self.var.trace(mode = "w", callback = self.showMsg)
        self.entry = tk.Entry(master = self, textvariable = self.var)
        self.btn = tk.Button(master = self, text = "Clear",
                             command = lambda: self.var.set(""))
        self.label = tk.Label(master = self)
        
        # pack() it up
        self.entry.pack()
        self.btn.pack()
        self.label.pack()
        """@code in here--------------------------------------------------"""

    """@function in here--------------------------------------"""

    def showMsg(self, *args):
        value = self.var.get()
        text = f"The Great One, {value if value else ''}!"
        self.label.config(text = text)
        return args
    
    """@function in here--------------------------------------"""


if __name__ == '__main__':
    app = TextChgApp()
    app.title("The Great one")
    app.mainloop()
