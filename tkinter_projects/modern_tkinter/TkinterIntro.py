from tkinter import *
from tkinter import ttk

def setEntry(*args):
    entry1_txt.set("Alias111")
    return args

def chkBtnChg(*args):
    entry1_txt.set(chk_btn1_txt.get())
    return args

def radioChg(*args):
    entry1_txt.set(radiobtn1_val.get())
    return args

def comboChg(*args):
    entry1_txt.set(combo1_val.get())
    return args


root = Tk()
root.title("Widget Example")

frame = ttk.Frame(root, padding = "10 10 10 10")
frame.grid(column = 0, row = 0, sticky = (N, S, E, W))

# Applies to resizing the window
root.columnconfigure(0, weight = 1)
root.rowconfigure(0, weight = 1)

"""@attention Label-Examples"""
label1_txt = StringVar()
label_1 = ttk.Label(frame, text = "Data :")
label_1.grid(column = 1, row = 1, sticky = (W, E))

# How to change a value for a label
t = "textvariable"
data = "Data"
label_1[t] = label1_txt
label1_txt.set(data)

"""@attention To Display Changes To Other Widgets"""
entry1_txt = StringVar()
entry_1 = ttk.Entry(frame, width = 7, textvariable = entry1_txt)

# Have the grid stretch horizontally
entry_1.grid(column = 2, row = 1, sticky = (W, E))
entry1_txt.set(label1_txt.get())

# calls the setEntry() function
btn_1 = ttk.Button(frame, text = "Click", command = setEntry)
btn_1.grid(column = 3, row = 1, sticky = (W, E))

# NOTE: To disable a button
# or enable a button
# btn_1["state"] = "disabled"
# btn_1["state"] = "enable"

"""@attention Working with check buttons"""
chk_btn1_txt = StringVar()
chk_btn1 = ttk.Checkbutton(
        frame, text = "The Great One",
        command = chkBtnChg, variable = chk_btn1_txt,
        onvalue = "Feliz", offvalue = "Mal"
        )
chk_btn1.grid(column = 4, row = 1, sticky = (W, E))

"""@attention Working with radio-buttons"""
radiobtn1_val = StringVar()
red_radiobtn = ttk.Radiobutton(frame, text = "Red", variable = radiobtn1_val,
                               value = "Red", command = radioChg)

blue_radiobtn = ttk.Radiobutton(frame, text = "Blue", variable = radiobtn1_val,
                                value = "Blue", command = radioChg)

green_radiobtn = ttk.Radiobutton(frame, text = "Green", variable = radiobtn1_val,
                                 value = "Green", command = radioChg)

red_radiobtn.grid(column = 2, row = 2, sticky = (W, E))
blue_radiobtn.grid(column = 3, row = 2, sticky = (W, E))
green_radiobtn.grid(column = 4, row = 2, sticky = (W, E))

label2 = Label(frame, text = "Fav Color")
label2.grid(column = 1, row = 2, sticky = (W, E))

"""Working with combo-boxes: There a drop down"""
combo1_val = StringVar()
combo1 = ttk.Combobox(frame, textvariable = combo1_val)

label3 = ttk.Label(frame, text = "Size")
label3.grid(column = 1, row = 3, sticky = (W, E))
# Assigning values to our combo box
combo1["values"] = ("Small", "Medium", "Large")
combo1.grid(column = 2, row = 3, sticky = (W, E))
# Calling a function with the combo box
combo1.bind("<<ComboboxSelected>>", comboChg)


root.mainloop()
