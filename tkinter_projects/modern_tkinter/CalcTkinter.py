from tkinter import *
from tkinter import ttk

def getSum(*args):
    try:
        num1_val = float(num1.get())
        num2_val = float(num2.get())
        
        solution.set(num1_val + num2_val)
        # Error that states no value there
    except ValueError:
        pass
    return args


root = Tk()
root.title("Calculator")

frame = ttk.Frame(root, padding = "10 10 10 10")

# Grid manager
frame.grid(column = 0, row = 0, sticky = (N, W, E, S))
root.columnconfigure(0, weight = 1)
root.columnconfigure(0, weight = 1)

# getting the sum of two values
num1 = StringVar()
num2 = StringVar()
solution = StringVar()

# Entry box
num_1_entry = ttk.Entry(frame, width = 7, textvariable = num1)
num_1_entry.grid(column = 1, row = 1, sticky = (W, E))
ttk.Label(frame, text = "+") \
    .grid(column = 2, row = 1, sticky = (W, E))

num_2_entry = ttk.Entry(frame, width = 7, textvariable = num2)
num_2_entry.grid(column = 3, row = 1, sticky = (W, E))


ttk.Button(frame, text = "Add", command = getSum) \
    .grid(column = 1, row = 2, sticky = (W, E))

solution_entry = ttk.Entry(frame, width = 7, textvariable = solution)
solution_entry.grid(column = 3, row = 2, sticky = (W, E))

num_1_entry.focus()

root.bind("<Return>", getSum)

root.mainloop()
