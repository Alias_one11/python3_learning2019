from tkinter import *
from tkinter import ttk

screenW = Tk()
screenW.title("Python Gui")

# Modify adding a label
aLabel = ttk.Label(screenW, text = "A Label")
aLabel.grid(column = 0, row = 0)

# Button click event function
def clickMe():
    action.configure(text = f"Hola {name.get()}")


ttk.Label(screenW, text = "Enter a name:").grid(column = 1, row = 0)

name = StringVar()
nameEntered = Entry(screenW, width = 12, textvariable = name)
nameEntered.grid(column = 0, row = 1)

action = Button(screenW, text = "Click Me!", command = clickMe)
action.grid(column = 1, row = 1)

screenW.mainloop()
