import urllib.request as request
import json
from python3Learning.src.oopBasics.MyFuncs \
    import formatObjs

def main():
    """code-here ----------------------------------"""
    my_url = 'http://data.nba.net/prod/v2/2018/teams.json'
    ERROR = "An error occurred while attempting to" \
            " retrieve data from the API."
    league = "league"
    standard = "standard"
    is_nbaf = "isNBAFranchise"
    
    with request.urlopen(my_url) as response:
        if response.getcode() == 200:
            src = response.read()
            data = json.loads(src)
        else:
            print(ERROR)
            
    formatObjs("data[league].keys()", data[league].keys())
    
    print()
    
    formatObjs("len(data[league][standard])",
               data[league][standard][4])

    nba_teams = [team for team in data[league][standard]
                 if team[is_nbaf]]
    
    with open("nba_team.json", 'w') as f:
        result = json.dump(nba_teams, f, indent = 4, sort_keys = True)

    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
