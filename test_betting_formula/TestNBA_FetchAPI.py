from typing import Dict
import requests
from python3Learning.src.oopBasics.MyFuncs \
    import formatObjs

def main():
    """code-here ----------------------------------"""
    url: str = "https://free-nba.p.rapidapi.com/stats"
    query_str: Dict[str, str] = {"page": "0", "per_page": "25"}
    
    host: str = "x-rapidapi-host"
    rapidapi_url: str = "free-nba.p.rapidapi.com"
    api: str = "x-rapidapi-key"
    api_key: str = "c42c9a4cfamsh2a91a456049468ap1b7d00jsnc826e818e0d8"
    GET: str = "GET"
    
    my_headers: Dict[str, str] = {
            host: rapidapi_url,
            api: api_key
            }
    
    response = requests.request(
            GET,
            url,
            headers = my_headers,
            params = query_str
            )
    formatObjs("Api Response Data", response.text)
    
    """code-here ----------------------------------"""


if __name__ == '__main__':
    main()
