from abc import abstractmethod, ABC

# EnterPrice Pattern: Specification
"""(ABCs)/(@abstractmethod) A decorator
indicating abstract classes/methods."""

class Specification(ABC):
    @abstractmethod
    def is_satisfied(self, item):
        raise NotImplementedError

class Filter(ABC):
    @abstractmethod
    def filter(self, item, spec):
        raise NotImplementedError

# Inherits from (Specification) & implements
# the methods from the abstract class
class ColorSpecification(Specification):
    def __init__(self, color):
        self.color = color
    
    def is_satisfied(self, item):
        return item.color == self.color

# Inherits from (Specification) & implements
# the methods from the abstract class
class BetterFilter(Filter):
    def filter(self, items, spec):
        for item in items:
            if spec.is_satisfied(item):
                yield item
