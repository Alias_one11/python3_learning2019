from python3Learning.src.oopBasics.MyFuncs import section
from python3Learning.src.solid_design_principles.open_close_principle.ClsProduct import Product
from python3Learning.src.solid_design_principles.open_close_principle.ClsSpecification_Filter import BetterFilter, \
    ColorSpecification
from python3Learning.src.solid_design_principles.open_close_principle.MyEnums import Size, Color

def main():
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Open Close Principle-Example")
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    apple = Product("Apple", Color.GREEN, Size.SMALL)
    tree = Product("Tree", Color.GREEN, Size.LARGE)
    house = Product("House", Color.BLUE, Size.LARGE)
    
    products = [apple, tree, house]
    
    # Creating an alias
    bFilter = BetterFilter()
    
    print("(New) green product:")
    green = ColorSpecification(Color.GREEN)
    
    returnGreen(bFilter, green, products)


if __name__ == '__main__':
    main()

def returnGreen(bFilter, green, products):
    for product in bFilter .filter(products, green):
        print(f" - [{product.name}] is GREEN ")
