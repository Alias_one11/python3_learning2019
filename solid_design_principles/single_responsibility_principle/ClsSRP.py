class Journal:
    count: int
    
    # constructor
    def __init__(self):
        self.entries = []
        self.count = 0
    
    # Adds an entry
    def add_entry(self, text):
        self.count += 1
        self.entries \
            .append(
                f"------------------------------------------------\n"
                f"(| Journal Entry |):\n"
                f"[ {self.count} ]: {text}\n"
                f"------------------------------------------------"
                )
    
    # Removes an entry at a certain position
    def remove_entry(self, pos):
        del self.entries[pos]
    
    def __str__(self) -> str:
        msg = '\n'.join(self.entries)
        print(msg)
        return msg
