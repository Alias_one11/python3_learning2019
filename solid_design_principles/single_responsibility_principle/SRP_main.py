from python3Learning.src.oopBasics.MyFuncs import section
from python3Learning.src.solid_design_principles.single_responsibility_principle.ClsSRP import Journal

def main():
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    section("Single Responsibility Principle-Example")
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    journal = Journal()
    # Adding entries to the journal
    journal.add_entry("Today was my third week not eating meat!..")
    journal.add_entry("Worked out like a mad man!..")
    journal.add_entry("Have to go grocery shopping!..")
    journal.add_entry("Follow @Alias_one11!..")
    
    # using our toString() method to output the entries
    journal.__str__()
    
    
if __name__ == '__main__':
    main()
