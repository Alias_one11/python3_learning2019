import lxml.html
import requests
import re
from selectolax.parser import HTMLParser
from bs4 import BeautifulSoup

def extractLinkRegEx(txt):
    tgs = re.compile(pattern = r"<a[^<>]+?href=([\'\"])(.*?)\1",
                     flags = re.IGNORECASE)
    return [match[1] for match in tgs.findall(txt)]

def extractLinksLxml(txt):
    lst = []
    dom = lxml.html.fromstring(txt)
    for _ in dom.xpath("//a/@href"):
        lst.append(_)
    return lst

def extractLinksHtmlParser(txt):
    lst = []
    dom = HTMLParser(txt)
    for tag in dom.tags('a'):
        attrs = tag.attributes
        if "href" in attrs:
            lst.append(attrs["href"])
    return lst

def extractBs(txt):
    lst = []
    s = BeautifulSoup(txt, "lxml")
    for tag in s.find_all('a', href = True):
        lst.append(tag["href"])
    return lst
    

"""@function to print out the content line by line"""
def printList(lst):
    for _ in lst:
        print(f"Level 1 -> {_}")


# Getting the request
r = requests.get("https://edfreitas.me")
# printList(extractLinkRegEx(r.text))
# printList(extractLinksLxml(r.text))
# printList(extractLinksHtmlParser(r.text))
printList(extractBs(r.text))
